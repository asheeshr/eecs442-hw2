%%
disp('Linear Least Squares 8 Point Algorithm')
set = 'set1';

[x1, x2] = readTextFiles(set);
image1 = imread(strcat(set, '/image1.jpg'));
image2 = imread(strcat(set, '/image2.jpg'));

subplot(2,2,1);
imshow(image1);
hold on;
plot(x1(1,:), x1(2,:), 'r+')
hold off;
subplot(2,2,2);
imshow(image2);
hold on;
plot(x2(1,:), x2(2,:), 'r+')
hold off;
suptitle(strcat('Linear Least Squares :',set));

x = zeros(length(x1), 9);
for i = 1:length(x1)
    x(i, :) = [
        x2(1,i)*x1(1,i) ...
        x2(1,i)*x1(2,i) ...
        x2(1,i)*x1(3,i) ...
        x2(2,i)*x1(1,i) ...
        x2(2,i)*x1(2,i) ...
        x2(2,i)*x1(3,i) ...
        x2(3,i)*x1(1,i) ...
        x2(3,i)*x1(2,i) ...
        x2(3,i)*x1(3,i) ...
        ];
end

[U,S,V] = svd(x);
F_temp = reshape(V(:,end),3,3)';
[U,S,V] = svd(F_temp);
S(end,end) = 0;
F = U*S*V';

lines2 = F*x1;
lines1 = F'*x2;

subplot(2,2,3);
imshow(image1);
hold on;
plot(x1(1,:), x1(2,:), 'r+')
error1 = zeros(length(lines1),1);
for i = 1:length(lines1)
    x = (x1(1,i)-50):(x1(1,i)+50);
    y = -lines1(1,i)/lines1(2,i)*x - lines1(3,i)/lines1(2,i) ;
    plot(x,y, 'b')
    error1(i) = abs(lines1(1,i)*x1(1,i) + lines1(2,i)*x1(2,i) + lines1(3,i)*x1(3,i))/sqrt(lines1(1,i)^2+lines1(2,i)^2);
    %refline(-lines1(1,i)/lines1(2,i), -lines1(3,i)/lines1(2,i));
end
hold off;
subplot(2,2,4);
imshow(image2);
hold on;
plot(x2(1,:), x2(2,:), 'r+')
error2 = zeros(length(lines1),1);
for i = 1:length(lines2)
    x = (x2(1,i)-50):(x2(1,i)+50);
    y = -lines2(1,i)/lines2(2,i)*x - lines2(3,i)/lines2(2,i) ;
    plot(x,y, 'b')
    error2(i) = abs(lines2(1,i)*x2(1,i) + lines2(2,i)*x2(2,i) + lines2(3,i)*x2(3,i))/sqrt(lines2(1,i)^2+lines2(2,i)^2);
    %refline(-lines1(1,i)/lines1(2,i), -lines1(3,i)/lines1(2,i));
end
hold off;

mean(error1)
mean(error2)

%%
disp('Normalized 8 Point Algorithm')
set = 'set1';

[x1, x2] = readTextFiles(set);
image1 = imread(strcat(set, '/image1.jpg'));
image2 = imread(strcat(set, '/image2.jpg'));

subplot(2,2,1);
imshow(image1);
hold on;
plot(x1(1,:), x1(2,:), 'r+')
hold off;
subplot(2,2,2);
imshow(image2);
hold on;
plot(x2(1,:), x2(2,:), 'r+')
hold off;
suptitle(strcat('Normalized 8 Point :',set));

tr1 = [mean(x1, 2)];
tr2 = [mean(x2, 2)];

D1 = sqrt(mean(((x1(1,:) - tr1(1)).^2 + (x1(2,:) - tr1(2)).^2).^0.5).^2);
scale1 = diag( [ sqrt(2)/D1, sqrt(2)/D1, 1 ] );% 2 pixels = RMSE Pixel Distance

D2 = sqrt(mean(((x2(1,:) - tr2(1)).^2 + (x2(2,:) - tr2(2)).^2).^0.5).^2);
scale2 = diag( [ sqrt(2)/D2, sqrt(2)/D2 , 1 ] ); % 2 pixels = RMSE Pixel Distance

trs1 = scale1 * [1 0 -tr1(1); 0 1 -tr1(2); 0 0 1];

trs2 = scale2 * [1 0 -tr2(1); 0 1 -tr2(2); 0 0 1];

x1_old = x1;
x2_old = x2;

x1 = trs1*x1;
x2 = trs2*x2;

x = zeros(length(x1), 9);
for i = 1:length(x1)
    x(i, :) = [
        x2(1,i)*x1(1,i) ...
        x2(1,i)*x1(2,i) ...
        x2(1,i)*x1(3,i) ...
        x2(2,i)*x1(1,i) ...
        x2(2,i)*x1(2,i) ...
        x2(2,i)*x1(3,i) ...
        x2(3,i)*x1(1,i) ...
        x2(3,i)*x1(2,i) ...
        x2(3,i)*x1(3,i) ...
        ];
end

x1 = x1_old;
x2 = x2_old;


[U,S,V] = svd(x);
F_temp = reshape(V(:,end),3,3)';
[U,S,V] = svd(F_temp);
S(end,end) = 0;
F = U*S*V';

F = trs2'*F*trs1;

lines2 = F*x1;
lines1 = F'*x2;


subplot(2,2,3);
imshow(image1);
hold on;
plot(x1(1,:), x1(2,:), 'r+')
error1 = zeros(length(lines1),1);
for i = 1:length(lines1)
    x = (x1(1,i)-50):(x1(1,i)+50);
    y = -lines1(1,i)/lines1(2,i)*x - lines1(3,i)/lines1(2,i) ;
    plot(x,y, 'b')
    error1(i) = abs(lines1(1,i)*x1(1,i) + lines1(2,i)*x1(2,i) + lines1(3,i)*x1(3,i))/sqrt(lines1(1,i)^2+lines1(2,i)^2);
    %refline(-lines1(1,i)/lines1(2,i), -lines1(3,i)/lines1(2,i));
end
hold off;
subplot(2,2,4);
imshow(image2);
hold on;
plot(x2(1,:), x2(2,:), 'r+')
error2 = zeros(length(lines1),1);
for i = 1:length(lines2)
    x = (x2(1,i)-50):(x2(1,i)+50);
    y = -lines2(1,i)/lines2(2,i)*x - lines2(3,i)/lines2(2,i) ;
    plot(x,y, 'b')
    error2(i) = abs(lines2(1,i)*x2(1,i) + lines2(2,i)*x2(2,i) + lines2(3,i)*x2(3,i))/sqrt(lines2(1,i)^2+lines2(2,i)^2);
    %refline(-lines1(1,i)/lines1(2,i), -lines1(3,i)/lines1(2,i));
end
hold off;

mean(error1)
mean(error2)

%%
disp('Stereo Rectification')
set = 'set1';

[x1, x2] = readTextFiles(set);
image1 = imread(strcat(set, '/image1.jpg'));
image2 = imread(strcat(set, '/image2.jpg'));

subplot(2,2,1);
imshow(image1);
hold on;
plot(x1(1,:), x1(2,:), 'r+')
hold off;
subplot(2,2,2);
imshow(image2);
hold on;
plot(x2(1,:), x2(2,:), 'r+')
hold off;
suptitle(strcat('Stereo Rectification :',set));
% 
% epipole1 = null(F);
% epipole1 = epipole1./epipole1(3);
% epipole1(3) = 1.000;
% temp = [1 0] - epipole1(1:2)';
% h1 = [
%     1 0 temp(1);
%     0 1 temp(2);
%     0 0 1
%     ];
% h1*epipole1

epipole2 = null(F');
epipole2 = epipole2./epipole2(3);
epipole2(3) = 1.000;
%temp = [1 0] - epipole2(1:2)';
% h1 = [
%     1 0 temp(1);
%     0 1 temp(2);
%     0 0 1
%     ];
theta = atan2(epipole2(2), epipole2(1));
h1 = [
    cos(-theta) -sin(-theta) 0;
    sin(-theta) cos(-theta) 0;
    0 0 1;
    ];

rot_epipole2 = h1*epipole2;
h2 = [
    1 0 0;
    0 1 0;
    -1/rot_epipole2(1) 0 1;
    ];

h2*h1*epipole2;

H_prime = h2*h1;


% Result 9.15 - HZ
P1 = [
    1 0 0 0;
    0 1 0 0;
    0 0 1 0;
    ];

epipole2_cross = [
    0 -epipole2(3) epipole2(2);
    epipole2(3) 0 -epipole2(1);
    -epipole2(2) epipole2(1) 0;
    ];
v = [1 1 1]'; 
P2 = [epipole2_cross*F + epipole2*v'];% epipole2];

% Section 11.12.2
H_zero = H_prime*P2;

% Solving Ax = b
x1_hat = H_zero*x1;
x2_hat = H_prime*x2;

x1_hat = [
    x1_hat(1,:)./x1_hat(3,:)
    x1_hat(2,:)./x1_hat(3,:)
    x1_hat(3,:)./x1_hat(3,:)
    ];

x2_hat = [
    x2_hat(1,:)./x2_hat(3,:)
    x2_hat(2,:)./x2_hat(3,:)
    x2_hat(3,:)./x2_hat(3,:)
    ];

A = x1_hat';
b = x2_hat(1,:)';

abc = inv(A'*A)*A'*b;
%[U,S,V] = svd(A);
%b_prime = U'*b;
%y = b_prime/diag(S);
%abc = V*y'

H_a = [
    abc(1) abc(2) abc(3);
    0 1 0;
    0 0 1;
    ];

H = H_a*H_zero;

x1_hat = H*x1;
x1_hat = [
    x1_hat(1,:)./x1_hat(3,:)
    x1_hat(2,:)./x1_hat(3,:)
    x1_hat(3,:)./x1_hat(3,:)
    ];

subplot(2,2,3);
tform = projective2d(H');
imshow(imwarp(image1, tform));
hold on;
%x1 = H'*x1;
%plot(x1_hat(1,:), x1_hat(2,:), 'r+')
%error1 = zeros(length(lines1),1);
%for i = 1:length(lines1)
%    x = (x1(1,i)-50):(x1(1,i)+50);
%    y = -lines1(1,i)/lines1(2,i)*x - lines1(3,i)/lines1(2,i) ;
%    plot(x,y, 'b')
%    error1(i) = abs(lines1(1,i)*x1(1,i) + lines1(2,i)*x1(2,i) + lines1(3,i)*x1(3,i))/sqrt(lines1(1,i)^2+lines1(1,i)^2);
    %refline(-lines1(1,i)/lines1(2,i), -lines1(3,i)/lines1(2,i));
%end
hold off;

subplot(2,2,4);
%imshow(image2);
tform = projective2d(H_prime');
imshow(imwarp(image2, tform));
hold on;
%x2 = H_prime'*x2;
%plot(x2(1,:), x2(2,:), 'r+')
% error2 = zeros(length(lines1),1);
% for i = 1:length(lines2)
%     x = (x2(1,i)-50):(x2(1,i)+50);
%     y = -lines2("Display window 2", 1,i)/lines2(2,i)*x - lines2(3,i)/lines2(2,i) ;
%     plot(x,y, 'b')
%     error2(i) = abs(lines2(1,i)*x2(1,i) + lines2(2,i)*x2(2,i) + lines2(3,i)*x2(3,i))/sqrt(lines2(1,i)^2+lines2(1,i)^2);
%     %refline(-lines1(1,i)/lines1(2,i), -lines1(3,i)/lines1(2,i));
% end
hold off;

dist = ( ...
(x1_hat(1,:) - x2_hat(1,:)).^2 + ...
(x1_hat(2,:) - x2_hat(2,:)).^2).^0.5;
error = sqrt(mean((dist).^2))